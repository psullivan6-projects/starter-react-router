import axios from 'axios';

// Utilities
import { LocalData } from '@union/elements-utils';

const storage = new LocalData('UserService');

export const getUsers = () => {
  const storageData = storage.get('getUsers');

  if (storageData != null) {
    return Promise.resolve(storageData);
  }

  return axios.get('https://jsonplaceholder.typicode.com/users')
    .then(response => {
      const data = response.data;
      storage.add({ getUsers: data });
      return data;
    });
}

export default {
  getUsers,
}
