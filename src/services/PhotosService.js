import axios from 'axios';

// Utilities
import { LocalData } from '@union/elements-utils';

const storage = new LocalData('photosService');

export const getPhotos = () => {
  const storageData = storage.get('getPhotos');

  if (storageData != null) {
    return Promise.resolve(storageData);
  }

  return axios.get('https://jsonplaceholder.typicode.com/photos')
    .then(response => {
      const data = response.data.slice(0, 10);
      storage.add({ getPhotos: data });
      return data;
    });
}

export default {
  getPhotos,
}
