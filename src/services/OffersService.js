// Utilities
import { LocalData } from '@union/elements-utils';

const storage = new LocalData('OffersService');

export const getOffers = () => {
  const storageData = storage.get('getOffers');

  if (storageData != null) {
    return Promise.resolve(storageData);
  }

  // Contrived example to show a mock delayed API response
  const prom = new Promise((resolve, reject) => {
    setTimeout(() => {
      const data = ['asdfasdf', '21341234', '12kjbgk12k3b', 'asd12ebads7'];
      storage.add({ getOffers: data });
      return resolve(data);
    }, 3000);
  });

  return prom;
}

export default {
  getOffers,
}
