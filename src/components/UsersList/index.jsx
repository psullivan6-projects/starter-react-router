import React from 'react';

const UsersList = ({ userData }) => (
  <ul>
    {
      userData.map(({ id, name }) => (<li key={id}>{name}</li>))
    }
  </ul>
);

export default UsersList;
