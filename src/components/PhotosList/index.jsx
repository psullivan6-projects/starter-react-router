import React from 'react';

const PhotosList = ({ photosData }) => (
  <ul>
    {
      photosData.map(({ id, thumbnailUrl, title }) => (
        <li key={id}>
          <img src={thumbnailUrl} alt={title} />
        </li>
      ))
    }
  </ul>
);

export default PhotosList;
