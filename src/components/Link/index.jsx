import React from 'react';

// Utilities
import history from '../../utilities/router-history';


const Link = ({ href, children, ...rest }) => {
  const onClick = (e) => {
    const aNewTab = e.metaKey || e.ctrlKey;
    const anExternalLink = href.substring(0, 4) === 'http';

    if (!aNewTab && !anExternalLink) {
      e.preventDefault();
      history.push(href);
    }
  };

  return (
    <a href={href} onClick={onClick} {...rest}>
      {children}
    </a>
  );
};

export default Link;
