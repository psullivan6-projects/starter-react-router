import React from 'react';

// Components
import { Link } from 'react-router-dom';

const Nav = () => (
  <ul>
    <li>
      <Link to="/">Home Page</Link>
    </li>
    <li>
      <Link to="/foo">Foo Page</Link>
    </li>
    <li>
      <Link to="/offers">Offers Page</Link>
    </li>
  </ul>
);

export default Nav;
