import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

// Services
import PhotosService from 'Services/PhotosService';
import UserService from 'Services/UserService';

// Components
import Loader from 'Components/Loader';

// Context
import { PageProvider } from 'Context/PageContext';

class Page extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading : true,
      setData : this.setData,
    }
  }

  componentDidMount() {
    // Default data promises
    const photosPromise = PhotosService.getPhotos();
    const userPromise = UserService.getUsers();

    Promise.all([
      userPromise,
      photosPromise,
    ])
      .then((
        [
          userData,
          photosData,
        ]
      ) => {
        this.setState({
          loading: false,
          userData,
          photosData,
        });
      })
      .catch((error) => {
        // Send an error log
      });
  }

  setData = (dataKey, dataPromise) => {
    this.setState({ loading: true });

    dataPromise.then(data => {
      this.setState({
        loading: false,
        [dataKey]: data
      });
    });
  }

  render() {
    const { loading } = this.state;
    const { children } = this.props;

    return (
      <PageProvider value={{ ...this.state }}>
        {
          (loading)
            ? <Loader />
            : (
              <Fragment>
                {children}
              </Fragment>
            )
        }
      </PageProvider>
    );
  }
}

Page.propTypes = {
  additionalData: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    promise: PropTypes.func,
  })),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
};

Page.defaultProps = {
  additionalData: [],
};

export default Page;
