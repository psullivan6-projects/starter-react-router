import { createContext } from 'react';

const PageContext = createContext();

export const PageConsumer = PageContext.Consumer;
export const PageProvider = PageContext.Provider;

export default PageContext;
