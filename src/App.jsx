import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Context
import { PageConsumer } from 'Context/PageContext';

// Components
import Page from 'Components/Page';

// Views
import Foo from './views/foo';
import Home from './views/home';
import Offers from './views/offers';

const App = () => (
  <Page>
    <PageConsumer>
      {
        (pageProps) => (
          <Router>
            <Route path="/" exact render={
              routerProps => <Home {...routerProps} {...pageProps} />
            } />
            <Route path="/foo/" render={
              routerProps => <Foo {...routerProps} {...pageProps} />
            } />
            <Route path="/offers/" component={
              routerProps => <Offers {...routerProps} {...pageProps} />
            } />
          </Router>
        )
      }
    </PageConsumer>
  </Page>
);

export default App;
