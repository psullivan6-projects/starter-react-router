import React from 'react';

const FourOhFour = () => (
  <section>
    <h1>FOUR OH FOUR</h1>
  </section>
);

export default FourOhFour;
