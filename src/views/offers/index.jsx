import React, { Component, Fragment } from 'react';

// Components
import Nav from 'Components/Nav';
import UsersList from 'Components/UsersList';

// Services
import { getOffers } from 'Services/OffersService';

class Offers extends Component {
  componentDidMount() {
    const { offersData, setData } = this.props;

    // Only get the data when it's needed, otherwise this will cause an infinite loop, since this
    // setData method causes a re-render
    if (offersData != null) { return; }

    setData('offersData', getOffers());
  }

  render() {
    const { offersData, userData } = this.props;

    return (
      <Fragment>
        <Nav />
        <h1 style={{ backgroundColor: '#f30' }}>OFFERS Page</h1>
        <h2>Custom Offers Data: {offersData}</h2>
        <UsersList userData={userData} />
      </Fragment>
    );
  }
};

export default Offers;
