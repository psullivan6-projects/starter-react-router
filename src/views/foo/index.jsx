import React, { Fragment } from 'react';

// Components
import Nav from 'Components/Nav';
import UsersList from 'Components/UsersList';


const Offers = ({ userData }) => (
  <Fragment>
    <Nav />
    <h1 style={{ backgroundColor: '#ff0' }}>FOO Page</h1>
    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto temporibus sed libero ipsa deleniti similique aliquid, illo in nisi dolorem officiis vero nemo doloremque laborum, fugit iste esse provident vitae!</p>
    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iusto temporibus sed libero ipsa deleniti similique aliquid, illo in nisi dolorem officiis vero nemo doloremque laborum, fugit iste esse provident vitae!</p>
    <UsersList userData={userData} />
  </Fragment>
);

export default Offers;
