## Quickstart

1. `yarn install`
2. `yarn start`
3. Watch the Network tab + navigate via links


## Articles

- https://hackernoon.com/routing-in-react-the-uncomplicated-way-b2c5ffaee997
- http://kcd.im/state


## Data Fetching

### TL;DR:

`page 1 | session 1` - GET from API  
`page 2 | session 1` - GET from memory (React "state" / Context)  
`page N | session 1` - GET from memory (React "state" / Context)  
...  
❌ EXIT session ❌  
`page 1 | session 2` - GET from localStorage  
`page 2 | session 2` - GET from memory  
`page N | session 2` - GET from memory  
...  

### In Detail

- 1st page request *of 1st session*
  - GET all the default data from API
  - GET any custom page data based on route from API
  - STORE all fetched data in localStorage (each service handles its own storage, since some data MUST always be re-requested)
- 2nd → Nth page request *of 1st session*
  - default data immediately available from state
  - GET any custom page data based on route

❌ EXIT session ❌

- 1st page request *of 2nd session*
  - GET all the default data from localStorage (or from API where required)
  - GET any custom page data from localStorage
- 2nd → Nth page request *of 2nd session*
  - default data immediately available from state
  - GET any custom page data from localStorage based on route


